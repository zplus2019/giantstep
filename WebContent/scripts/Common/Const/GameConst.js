var GameConst = (function () {
	  var instance;
	  function init() {
	    return {	      
	      getPopupServerMessage: function (index){
	    	  if(this.ErrorCode !== undefined && this.ErrorCode.properties !== undefined && this.ErrorCode.properties[index] !== undefined){
	    		  if(this.ErrorCode.properties[index].explain !== ""){
		    		  return this.ErrorCode.properties[index].explain;
		    	  }
		    	  return this.ErrorCode.properties[index].code;  
	    	  } else {
	    		  console.log("[Caution!!] Server Error is " + index);
	    	  }
	    	  return index.toString();	    	   
	      },
	    	  
	      //CharacterNames : ["가1", "가2", "가3", "나1", "너1", "너와나12"],
	      
//		  //enum
//		  //GameModeType : {AI : 0, VS_1 : 1},		  
//		  ErrorCode : {
//			  properties: {
//			    0: {code: "SUCCESS", explain: ""},
//			    
//			    200: {code: "LOGIN_PARAMETER", explain: "로그인에 문제가 발생했습니다.\n게임을 종료합니다."},
//			  }
//			},		
	    };
	  }

	  return {
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        //console.log( "The GameConst init!!!" );
	      } else {
	    	  //console.log( "The GameConst instance!" );
		  }
	      return instance;
	    }
	  };
	})();

