var GameUtil = (function () {
	  var instance;
	  function init() {
	    return {
	    	//배열에서 하나의 인덱스를 삭제한다.
	      removeAt : function(aArray, aIdx) {
	      	return aArray.splice(aIdx, 1);
	      },

	      //오브젝트가 해당 축에 있는지 판단한다.
	      checkCollisonX : function(aCx1, aCx2, aKx1, aKx2) {
	      	if(	(aCx1 <= aKx1 && aKx1 <= aCx2)	|| (aCx1 <= aKx2 && aKx2 <= aCx2) || (aCx1 >= aKx1 && aKx2 >= aCx2) )	{
	      		return true;
	      	}
	      	return false;
	      },

	      //오브젝트가 충도아는지 판단한다.
	      checkCollision : function(aCx1, aCx2, aKx1, aKx2, aCy1, aCy2, aKy1, aKy2) {
	      	if(	(aCx1 <= aKx1 && aKx1 <= aCx2)	|| (aCx1 <= aKx2 && aKx2 <= aCx2) || (aCx1 >= aKx1 && aKx2 >= aCx2) ) {
	      		if(	(aCy1 <= aKy1 && aKy1 <= aCy2)	|| (aCy1 <= aKy2 && aKy2 <= aCy2) || (aCy1 >= aKy1 && aKy2 >= aCy2) ) {
	      			return true;
	      		}
	      	}	
	      	return false;
	      },

	      //텍스트와 정렬 설정.
	      setText : function(aObject, aBoxWidth, aBoxHeight, aH, aV, aText){
	      	aObject.setTextBounds(0, 0, aBoxWidth, aBoxHeight);	
	      	aObject.boundsAlignH = aH;
	      	aObject.boundsAlignV = aV;
	      	
	      	if(aText !== undefined)
	      		aObject.text = aText;
	      },

	      //텍스트정렬 설정.
	      setTextAlign : function(aObject, aBoxWidth, aBoxHeight, aH, aV){
	      	aObject.setTextBounds(0, 0, aBoxWidth, aBoxHeight);
	      	aObject.boundsAlignH = aH;
	      	aObject.boundsAlignV = aV;	
	      },

	      //텍스트 색상 설정.
	      setTextColor : function(aObject, aFillColor, aStrokeColor){
	      	aObject.fill = aFillColor;
	      	aObject.stroke = aStrokeColor;
	      },
	      
	      //url 파라미터의 이름을 가지고 온다.	ex)var uid = getParameterByName('userid');
	      getParameterByName: function(name,href) {
	    	  if (!url) url = window.location.href;
	    	  //window.location.saerch	... 쿼리 부붑만 가져옴..
	    	    name = name.replace(/[\[\]]/g, "\\$&");
	    	    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
	    	        results = regex.exec(url);
	    	    if (!results) return null;
	    	    if (!results[0]) return '';
	    	    return decodeURIComponent(results[0].replace(/\+/g, " "));
	      },
	      
	      //쿠키를 가지고 온다.
	      getCookie: function(cname) {
	    	  var name = cname + "=";
	    	  var decodedCookie = decodeURIComponent(document.cookie);
	    	  var ca = decodedCookie.split(';');
	    	  for(var i = 0; i <ca.length; i++) {
	    	    var c = ca[i];
	    	    while (c.charAt(0) == ' ') {
	    	      c = c.substring(1);
	    	    }
	    	    if (c.indexOf(name) == 0) {
	    	      return c.substring(name.length, c.length);
	    	    }
	    	  }
	    	  return "";
	    	},
	    	
	    	//쿠키를 셋팅한다.
	    	setCookie : function (cname, cvalue, exdays) {
	    		  var d = new Date();
	    		  d.setTime(d.getTime() + (exdays*24*60*60*1000));
	    		  var expires = "expires="+ d.toUTCString();
	    		  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
	    		},
	    		
	    	//쿠키를 지운다.
	    	delCookie : function(cname, domain){
	    		//document.cookie = "accepted_invitation=; expires=Thu, 01 Jan 1970 00:00:00 UTC; domain=.malangmalang.com; path=/";
	    		var expires = "expires=Thu, 01 Jan 1970 00:00:00 UTC";
	    		var domain = "domain=" + domain;
	    		
	    		document.cookie = cname + "=; " + expires + ";" +  ";path=/";
	    	},
	    	
	    	//delCookie(accepted_invitation, ".malangmalang.com");
	      
	    	//쿼리를 가지고 온다.
	      getQuery: function(needle) {
	    	    if (needle === undefined || needle === null) {
	    	        return null;
	    	    }

	    	    var
	    	        items = {},
	    	        queries = {};

	    	    if (window.location.search) {
	    	        items = window.location.search.substr(1).split('&');
	    	    }

	    	    for (var key in items) {
	    	        var fragments = items[key].split('=');
	    	        queries[fragments[0]] = fragments[1];
	    	    }

	    	    if (queries[needle]) {
	    	        return queries[needle];
	    	    }

	    	    return null;
	    	}
	      
	    };
	  }

	  return {
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        //console.log( "The GameUtil init!!!" );
	      } else {
	    	  //console.log( "The GameUtil instance!" );
		  }
	      return instance;
	    }
	  };
	})();