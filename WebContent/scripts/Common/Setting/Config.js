var urlType;

var Config = (function () {
	  var instance;
	  function init() {
	    return {
	    	
	    	setURLType : function(){
	             var site = window.location.host;
	             console.log("site = " + site);
	             if (site.indexOf('localhost') != -1) {
	                urlType = this.URL_TYPE.SERVER_LOCAL;
	              } else if (site.indexOf("129.168.0") != -1) {
	                 urlType = this.URL_TYPE.SERVER_LOCAL;
	              } else {
	                 urlType = this.URL_TYPE.LIVE;
	              }
	          },
	          
	    	getURLType : function(){
	    		return urlType;
	    	},
	    	
	    	URL_TYPE : {SERVER_LOCAL : 0, LIVE : 1},
	    	URL : {
				  properties: {
				    0: {api_home_domain: "http://192.168.0.29:3000/"},	//SERVER_LOCAL			    
				    1: {api_home_domain: "http://192.168.0.29:3000/"}	//LIVE
				  } 
	    	},
    	
	    	getAPIURL : function(){	    		
	    		return this.URL.properties[urlType].api_home_domain;
	    	}
	    	
	    };
	  }

	  return {
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        instance.setURLType();
	        //console.log( "The Config init!!!" );
	      } else {
	    	  //console.log( "The Config instance!" );
		  }
	      return instance;
	    }
	  };
	})();