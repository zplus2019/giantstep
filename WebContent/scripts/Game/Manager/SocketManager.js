var socket;
var gameURL;

var SocketManager = (function () {
	  // Instance stores a reference to the Singleton
	  var instance;
	
	  function init() {
	    return {
	    	//=================================== Common ===========================================
	    	
		  	  isSocketOnCommon : false,
			  //isSocketOnLobby : false,
			  //isSocketOnGame : false,
		  
	    	
	    	gameData : GameData.getInstance(),
		    
	    	//소켓커넥트.	    	
	    	setConnect : function(aSuccessCallBack){
	    		if(this.gameData.gameURL === undefined || this.gameData.gameURL === null || this.gameData.gameURL === ""){
	    			//console.log("SocketManager :: setConnect -> this.gameData.gameURL is undefined or null or empty");
	    			return;
	    		}	    		
	    		//console.log("SocketManager :: setConnect -> this.gameData.gameURL : " + this.gameData.gameURL);
	    		
	    		socket = io.connect(this.gameData.gameURL, {reconnection:false, transports:['websocket']} );
	    		//socket = io.connect(gameURL);
	    		//console.log("SocketManager :: setConnect -> init");
	    		//console.log("SocketManager :: setConnect -> this.isSocketOnCommon is " + instance.isSocketOnCommon);
	    		
	    		socket.on('connect', function(data) {
	    			//address = socket.request.connection.remoteAddress;
	    			//console.log("SocketManager :: setConnect -> connect 소켓이 연결됐습니다.");	    			
	    			//console.log("SocketManager :: SetConnet -> address.address : " + address);
	    			
	    			if(instance.isSocketOnCommon === false){
	    				instance.isSocketOnCommon = true;
		    			aSuccessCallBack();
		    		}
	    		});
	    		
	    		
	    	   
	    	   socket.on('disconnect', function(){
	    		   instance.isSocketOnCommon = false;
    			  //console.log("SocketManager :: disconnect");
	    			//window.setTimeOut(function(){self.close();}, 1000);
	    			//self.close();
	    		});
	    	},
	    	
	    	//DisConnect이벤트 호출.
	    	setDisConnect : function(){
	    			if(socket) socket.disconnect();
	    			instance.isSocketOnCommon = false;
	    			//console.log("SocketManager :: setDisConnet -> DisConnect");
	    			//window.setTimeOut(function(){self.close();}, 1000);
	    			//self.close();
	    		
	    	},
	    	
	    	getSocketOnCommon : function(){
	    		return instance.isSocketOnCommon;
	    	},	    	
	    	
	    	netSocketEmit : function(aEventName, aJsonObjectData){
	    		//reqEnterRoomData전송방식 : json object 객체 -> JSON.stringify : json 형태의 string -> encodeURIComponent : UTF8 encoder
	    		var jsonStringReqData = JSON.stringify(aJsonObjectData);
	    		
	    		if(socket === undefined || socket === null){
	    			//console.log("SocketManager :: socket is undefined!!!");
	    			return;
	    		}	    		
	    		
	    		socket.emit(aEventName, encodeURIComponent(jsonStringReqData)  );
	    		//console.log("SocketManager :: netSocketEmit -> [EvnetName] " + aEventName + " [Data] " + jsonStringReqData);
	    	},
	    	
	    	netSocketOn : function(aEventName, aSuccessCallBack, aFailCallBack){
	    		socket.on(aEventName, function(data){
	    			//data수신방식 : UTF8 encode 된 상태 -> decodeURIComponent : UTF8 decode... -> JSON.parse : json string 를 json Object의 객체로 변경.
	    			var jsonStringData = decodeURIComponent(data);
	    			var jsonParserData = JSON.parse(jsonStringData);

	    			if(jsonParserData.result !== undefined && jsonParserData.result !== 0){
	    				//console.log("SocketManager :: netSocketOn -> ERROR!!! [EvnetName] " + aEventName + " [Data] " + jsonStringData);
	    	    		aFailCallBack(jsonParserData.result);
	    			} else {
	    				//console.log("SocketManager :: netSocketOn -> SUCCESS!!! [EvnetName] " + aEventName + " [Data] " + jsonStringData);
	    				aSuccessCallBack(jsonParserData);
	    			}
	    		});
	    	},
	    	//======================================================================================
	    	
	    	//==================================== Lobby ===========================================
	    	netReqLogin : function(aMsgIdx, aNickName, aSessionID, aCharacterType, aUUid){
	    		this.netSocketEmit(aMsgIdx, {
    				"msg_idx" : aMsgIdx,
    				"nick_name" : aNickName,
    				"session_id" : aSessionID,    				
    				"character_type" : aCharacterType,
    				"uuid" : aUUid
	    		});
	    	},
	    	
	    	netResLogin : function(aSuccessCallBack, aFailCallBack){
 	    		this.netSocketOn('res_login', aSuccessCallBack, aFailCallBack);
 	    	},
 	    	//====================================================================================== 	    	
 	    	
 	    	
	    	
	    	//=================================== InGame =========================================== 	    		
 	    	netReqGameReady : function(aMsgIdx, aSessionID, aRoomNum, aNickName){
 	    		this.netSocketEmit(aMsgIdx, {
 	    			"msg_idx" : aMsgIdx,
 	    			"session_id" : aSessionID,
 	    			"room_number" : aRoomNum,
 	    			"nick_name" : aNickName
 	 	    	});
	    	},
	    	
	    	netResGameReady : function(aSuccessCallBack, aFailCallBack){
	    		this.netSocketOn('res_game_ready', aSuccessCallBack, aFailCallBack);
	    	},	    		
	    	//======================================================================================
	    };
	  }

	  return {
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        //instance.setURL();
	        //console.log( "The SocketManager init!!!" );
	      } else {
	    	  //console.log( "The SocketManager instance!" );
		  }
	      return instance;
	    }
	  };
	})();
