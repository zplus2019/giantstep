var soundArray = [];

var SoundManager = (function() {
	// Instance stores a reference to the Singleton
	var instance;	
	var that;
	
	function init() {
		return {
			gameData : this.GameData.getInstance(),
			SoundNames : ["testClick", "testClick"],
			                        
            SOUND_TYPE : {testClickLoop : 0, testClick : 0},

			setSoundArray : function(){
				for(var i = 0; i < this.SoundNames.length; i++){
					soundArray.push(that.game.add.audio(this.SoundNames[i]));
				}
			},
			
			play : function(aSoundName) {
				// BGM의 경우...'Coin_Music'
					var volume = 0.5;					
					if((aSoundName === this.SOUND_TYPE.testClickLoop) ){
						volume = (this.gameData.backgroundVolume / 100.0);
						soundArray[aSoundName].loop = true;
					} else {
						volume = (this.gameData.effectVolume / 100.0);
						soundArray[aSoundName].loop = false;
					}
					soundArray[aSoundName].volume = volume;
					soundArray[aSoundName].play();
			},

			stop : function(aSoundName) {
				soundArray[aSoundName].stop();			
			},
			
			pause : function(aSoundName) {
				soundArray[aSoundName].pause();			
			},
			
			resume : function(aSoundName) {
				var volume = 0.5;					
				if((aSoundName === this.SOUND_TYPE.testClickLoop) ){
					volume = (this.gameData.backgroundVolume / 100.0);
					soundArray[aSoundName].loop = true;
				} else {
					volume = (this.gameData.effectVolume / 100.0);
						soundArray[aSoundName].loop = false;
				}
				soundArray[aSoundName].volume = volume;
				soundArray[aSoundName].resume();			
			},

		};
	}

	return {
		getInstance : function(_that) {			
			if (!instance) {
				instance = init();
				that = _that;
				instance.setSoundArray();
				//console.log("The SoundManager init!!!");
			} else {
				//console.log("The SoundManager instance!");
			}
			return instance;
		}
	};
})();
