window.onload = function() {
//	var game = new Phaser.Game(1024, 576, Phaser.AUTO);
//	var game = new Phaser.Game(1024, 576, Phaser.WEBGL);
	var game = new Phaser.Game(2000, 1050, Phaser.CANVAS);
	
	// Add the States your game has.
	game.state.add("BootV1", BootV1);
	game.state.add("PreloaderV1", PreloaderV1);
	//game.state.add("Menu", Menu);
	game.state.add("MenuV1", MenuV1);
	
	game.state.add("LevelV1", LevelV1);
	
	game.state.start("BootV1");
};