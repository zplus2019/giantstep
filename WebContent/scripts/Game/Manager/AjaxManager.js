var channels_url;
var session_id_login_url;
var auto_enter_url;
var request_room_total_count_url;
var request_info_save_url;
//  
//Data - Request
var reqSessionIDLoginData = {
	"session_id" : "Test001",
};
  
//자동입장.
var reqAutoEnterData = {
	"no_send": 0,
	"server_idx": 0
};

var reqLoadRoomCountData = {
	"server_idx": 0,	
};

var reqInfoSaveData = {
    "identityNumber" : "",
    "name" : "",
	"team_name" : ""
};

var AjaxManager = (function () {
	  var instance;
	  
	  
	  
	  function init() {
	    return {
	    	
        config : Config.getInstance(),        
	    setURL : function(){
	    	channels_url = this.config.getAPIURL() + "web/v1/channels";
	    	session_id_login_url = this.config.getAPIURL() + "web/v1/login";
	  	    auto_enter_url = this.config.getAPIURL() + "setcoin/auto_enter";
	  	    request_room_total_count_url = this.config.getAPIURL() + "setcoin/get_total_room_list";
	  	    
	  	    request_info_save_url = this.config.getAPIURL() + "api/v1/SecEdu";
	    },	
	    	  
    	//================================== Request ===========================================
	      netReqChannelUrl : function(aChannelID, aSuccessCallBack, aFailCallBack){
	    	var channelurl = channels_url + "/" + aChannelID.toString();
      		var data = aChannelID;      		
      		
      		this.netReqAjaxGet(channelurl, data, "", instance.netResChannelUrl, this.netResError, aSuccessCallBack, aFailCallBack);
    	  }, 
	    
	  	  netReqSessionIDLogin : function(aSessionID, aSuccessCallBack, aFailCallBack){
	  		var bearerToken =  aSessionID;
      		var data = "";
      		
      		this.netReqAjaxGet(session_id_login_url, data, bearerToken.toString(), instance.netResSessionIDLogin, this.netResError, aSuccessCallBack, aFailCallBack);
	  	  },
    	  
    	  netReqAutoEnter : function(aChannelIndex, aSuccessCallBack, aFailCallBack){	
    		reqAutoEnterData.no_send = 0;    
    		reqAutoEnterData.server_idx = aChannelIndex;
    		this.netReqAjax(auto_enter_url, reqAutoEnterData, instance.netResAutoEnter, this.netResError, aSuccessCallBack, aFailCallBack);
      	  },
      	  
      	  netReqLoadRoomCount : function(aChannelIndex, aSuccessCallBack, aFailCallBack){	
    		reqLoadRoomCountData.server_idx = aChannelIndex;    		

    		var jsonPack = {};    		
    		var ciphertext = CryptoJS.AES.encrypt(JSON.stringify(reqLoadRoomCountData), "dhwkekdyend123dsr23");
    		jsonPack.packet = "" + ciphertext;    		

    		this.netReqAjax(request_room_total_count_url, jsonPack, instance.netResLoadRoomCount, this.netResError, aSuccessCallBack, aFailCallBack);
      	  },
      	  
      	  // 자이언트 스텝 정보 보내기
      	  netReqInfoSave : function(aInfoNumber, aInfoName, aInfoTeam, aSuccessCallBack, aFailCallBack){	
      		reqInfoSaveData.identityNumber = aInfoNumber;    
      		reqInfoSaveData.name = aInfoName;
      		reqInfoSaveData.team_name = aInfoTeam;
      		
    		this.netReqAjax(request_info_save_url, reqInfoSaveData, instance.netResInfoSave, this.netResError, aSuccessCallBack, aFailCallBack);
      	  },
      	 
      	
    	  //post
    	  netReqAjax : function(aUrl, aJsonObjectData, aResCallBack, aErrorCallBack, aSuccessCallBack, aFailCallBack){
    	  	
    	  	//console.log("AjaxManager :: netReqAjax -> [url] " + aUrl + " [Data] " + JSON.stringify(aJsonObjectData));
    	  	
    	  	$.ajax({
    	          type: 'post',        
    	          url: aUrl,
    	          data: aJsonObjectData,
    	          dataType:'json',
    	          success: function (data) {
    	          	aResCallBack(aUrl, data, aSuccessCallBack, aFailCallBack);
    	          },
    	          error: function (response, status, error) {
    	        	aErrorCallBack(aUrl, response, status, error, aFailCallBack);
    	          }
    	      });
    	  },
    	  
    	  //get	Authorization;
    	  netReqAjaxGet : function(aUrl, aData, aBearerToken, aResCallBack, aErrorCallBack, aSuccessCallBack, aFailCallBack){    		  
      	  	//console.log("AjaxManager :: netReqAjaxGet -> [url] " + aUrl + " [aData] " + aData + " [aBearerToken] " + aBearerToken);
      	  	var bearerToken = (aBearerToken === "") ? "" : ('Bearer ' + aBearerToken);
      	  	
      	  	$.ajax({
      	          type: 'get',     
      	          data: aData,
      	          url: aUrl,
	      	        headers: {
	      	          "Authorization": bearerToken,
	      	        },
      	          success: function (data) {
      	          	aResCallBack(aUrl, data, aSuccessCallBack, aFailCallBack);
      	          },
      	          error: function (response, status, error) {
      	        	aErrorCallBack(aUrl, response, status, error, aFailCallBack);
      	          }
      	      });
      	  },
    	  //======================================================================================
    	  
    	//================================== Response ==========================================
      	  
    	  netResChannelUrl : function(aLoginURL, aData, aSuccessCallBack, aFailCallBack){
    		  instance.netResData(aLoginURL, aData, instance.setResChannelUrl, aSuccessCallBack, aFailCallBack);
    	  },

      	
    	  netResSessionIDLogin : function(aLoginURL, aData, aSuccessCallBack, aFailCallBack){
    		  instance.netResData(aLoginURL, aData, instance.setResSessionIDLogin, aSuccessCallBack, aFailCallBack);
    	  },
    	  
    	  netResAutoEnter : function(aAutoEnterURL, aData, aSuccessCallBack, aFailCallBack){
    		  instance.netResData(aAutoEnterURL, aData, instance.setResAutoEnter, aSuccessCallBack, aFailCallBack);
    	  },
    	  
    	  netResLoadRoomCount : function(aLoadRoomCountURL, aData, aSuccessCallBack, aFailCallBack){
    			
    			var decrypted = CryptoJS.AES.decrypt(aData.packet, 'dhwkekdyend123dsr23');
    			var decryptedData = JSON.parse(decrypted.toString(CryptoJS.enc.Utf8));
    		    
        		instance.netResData(aLoadRoomCountURL, decryptedData, instance.setResLoadRoomCount, aSuccessCallBack, aFailCallBack);
      	  },
      	  
      	  // 자이언트 스텝 정보 보낸 결과 받기
      	  netResInfoSave : function(aInfoSaveURL, aData, aSuccessCallBack, aFailCallBack){
      		  instance.netResData(aInfoSaveURL, aData, instance.setResInfoSave, aSuccessCallBack, aFailCallBack);
      	  },
      	  
    	  netResData : function(aUrl, aData, aResCallback, aSuccessCallBack, aFailCallBack){    	  	
	    	  	if(aData.result !== undefined){
	    	  		if(aData.result !== 0){
	    	  			this.netResResultError(aData.result, aFailCallBack);
		    	  		return;
		    	  	}
	    	  	}
    	  	
    	  	aResCallback(aData, aSuccessCallBack);
    	  },

    	  //result error
    	  netResResultError : function(aResult, aFailCallBack){
    	  		//console.log("AjaxManager :: netResResultError -> [aResult] " + aResult);
    	  		aFailCallBack(aResult);
    	  },	

    	  //ajax error
    	  netResError : function(aUrl, aResponse, aStatus, aError, aFailCallBack){
    	  	console.log("AjaxManager :: netResError -> [Url] " + aUrl + " [Response] " + aResponse + " [Status] " + aStatus + " [ERROR] " + aError);
    	  	aFailCallBack(aResponse);
    	  },
    	  //======================================================================================
    	  
    	//=============================== SetGetResponseData ===================================
    	  setResChannelUrl : function(aResData, aSuccessCallBack){	
        		//resFriendListData = aResData;        		
        		aSuccessCallBack(aResData);
      	  },
      	  
    	  setResSessionIDLogin : function(aResData, aSuccessCallBack){	
    	  	resSessionIDLoginData = aResData;
    	  	aSuccessCallBack(aResData);
    	  },

    	  getResSessionIDLogin : function(){	
    	  	return resSessionIDLoginData;
    	  },
    	  
    	  setResAutoEnter : function(aResData, aSuccessCallBack){	
    		  resAutoEnterData = aResData;
    		  aSuccessCallBack(aResData);
      	  },

      	  getResAutoEnter : function(){	
      	  	return resAutoEnterData;
      	  },
      	  
      	  setResLoadRoomCount : function(aResData, aSuccessCallBack){	
      		resLoadRoomCountData = aResData;
      		aSuccessCallBack(aResData);
    	  },

    	  getResLoadRoomCount : function(){	
    	  	return resLoadRoomCountData;
    	  },
    	  
    	  setResInfoSave : function(aResData, aSuccessCallBack){	
          	resInfoSaveData = aResData;
            aSuccessCallBack(aResData);
      	  },
      	  
      	  getResInfoSave : function(){	
    	  	return resInfoSaveData;
      	  },
      	  
    	  //======================================================================================
	    };
	  }

	  return {
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        instance.setURL();
	        //console.log( "The AjaxManager init!!!" );
	      } else {
	    	 //console.log( "The AjaxManager instance!" );
		  }
	      return instance;
	    }
	  };
	})();