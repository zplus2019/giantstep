var invitationData; 

var GameData = (function () {
	  var instance;
	  function init() {
	    return {
			userNickName : "Test102",	
			backgroundVolume : 50,
			effectVolume : 80,
	    };
	  };

	  return {
	    // Get the Singleton instance if one exists
	    // or create one if it doesn't
	    getInstance: function () {
	      if ( !instance ) {
	        instance = init();
	        //console.log( "The GameData init!!!" );
	      } else {
	    	  //console.log( "The GameData instance!" );
		  }
	      return instance;
	    }
	  };
	})();

